from flask import jsonify, request
from db.db import cnx

class Empaque():
    global cur
    cur = cnx.cursor()

    def list():
        lista = []
        cur.execute("SELECT * FROM empaque")
        rows = cur.fetchall()
        columns = [i[0] for i in cur.description]
        for row in rows:
            #Create a zip object from two lists
            registro = zip(columns, row)
            #Create a dictionary from zip object
            json = dict(registro)
            lista.append(json)
        return jsonify(lista)
        cnx.close

    def create(body):
        #Campos:
        data = (body['tipo'],body['tamano'],body['cantidad'])
        #Setencia SQL:
        sql = "INSERT INTO empaque(tipo, tamano, cantidad) VALUES (%s, %s, %s)"
        print('sql', sql)
        cur.execute(sql, data)
        cnx.commit()
        return {'estado': 'Se insertó un nuevo empaque'}, 200