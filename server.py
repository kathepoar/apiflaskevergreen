from flask import Flask, jsonify, request
from flask_cors import CORS
from controllers.Empaque import Empaque

app = Flask(__name__)
CORS(app)

@app.route('/empaques', methods=['GET'])
def getAll():
    return (Empaque.list())

@app.route('/empaques', methods=['POST'])
def postOne():
    body = request.json
    return (Empaque.create(body))

app.run(port=5000, debug=True)