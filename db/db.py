import mysql.connector as mysql
from config import scr

cnx = mysql.MySQLConnection(
    host = "127.0.0.1",
    port = 3306,
    user = scr['username'],
    password = scr['secret'],
    database = "evergreen",
)